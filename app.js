//Coloumn and Row header
let arrRowHeader = [];

const row_inc = Math.ceil(window.screen.width / 35);
const col_inc = Math.ceil(window.screen.height / 10);

let row = 0;
let col = 0;
let colh = "";

let curRowLevel = row_inc;
let curColLevel = col_inc;

function getRow() {
  return document.createElement("tr");
}

function getCol() {
  return document.createElement("td");
}

function getNextChar(alpha) {
  let arrColHeader = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
  if (alpha == "Z") {
    return "A";
  } else {
    return arrColHeader[arrColHeader.indexOf(alpha) + 1];
  }
}

function getColHeader(header) {
  let headerLen = header.length;
  if (header == "") {
    return "A";
  } else if (headerLen == 1 && header != "Z") {
    return getNextChar(header);
  } else if (headerLen > 1 || header == "Z") {
    let arrHeader = header.split("").reverse();
    if (arrHeader[0] == "Z") {
      for (var i = 0; i < headerLen; i++) {
        arrHeader[i] = getNextChar(arrHeader[i]);
        if (arrHeader[i] != "Z") break;
      }
      arrHeader = arrHeader.reverse();
      arrHeader.push("A");
    } else {
      arrHeader[0] = getNextChar(arrHeader[0]);
      arrHeader.reverse();
    }
    return arrHeader.join("");
  }
}

//Function to create table at the start
function createTable() {
  let table = document.createElement("table");
  table.setAttribute("id", "table");
  let tr = table.insertRow(-1);

  // add table to a container.
  let span = document.getElementById("cont");
  span.appendChild(table);

  for (; row < curRowLevel + 1; row++) {
    for (; col < curColLevel + 1; col++) {
      if (row == 0 && col == 0) {
        //For first text box element
        tr.appendChild(getCol());
      } else if (row == 0 && col > 0) {
        //For col headers
        let th = document.createElement("th");
        //th.innerHTML = arrColHeader[col - 1];
        colh = getColHeader(colh);
        th.innerHTML = colh;
        tr.appendChild(th);
      } //For row headers
      else if (row > 0 && col == 0) {
        let tr = table.insertRow(row);
        td = getCol();
        td.innerHTML = row;
        tr.append(td);
      } //for rest of the cells
      else if (row > 0 && col > 0) {
        td = getCol();
        td.appendChild(document.createElement("input"));
        table.rows[row].appendChild(td);
      }
    }
    col = 0;
  }
}
