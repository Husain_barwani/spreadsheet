let arrRowHeader = [];
let colh = "";
let row = 0, col = 0;

const row_inc = window.screen.width;
const col_inc = window.screen.height;      //Initial value for grid rendering
const rowSpan = 20;  //height of cell
const colSpan = 75;  //width of cell


function getCanvas(n){
  const canvas = document.getElementById("cont"), 
  context = canvas.getContext("2d"); 
  switch (n){
    case 1 : return context; break;
    case 2 : return canvas; break;
    default : return [context, canvas]; 
  }
}  

const body = document.querySelector('body');
//Here r : is the width factor and c : height factor
function createGrid(row_start, col_start){
  const [context, canvas] = getCanvas();
  canvas.width = row_inc*2;
  canvas.height = col_inc*2;
  const divCol = document.createElement('div');
  divCol.id = 'div-col-header';
  body.appendChild(divCol);
  const divRow = document.createElement('div');
  divRow.id = 'div-row-header';
  body.appendChild(divRow);
  //Adds horizontal lines
  for (let r = row_start + rowSpan, rowh = 1; r < (row_inc + row_start + rowSpan*1000); r += rowSpan, rowh++){
    
    context.moveTo(0, r);
    context.lineTo(row_inc + row_start + rowSpan*1000, r);

    //Code for coloring first row in old_login.js

    let rowHeader = document.createElement('p');
    rowHeader.id = 'rowh' ;
    rowHeader.innerHTML = rowh;
    divRow.append(rowHeader);

    //Updating canvas height
    if(r == (row_inc + row_start + rowSpan*1000 - 1)){
      canvas.height += col_inc*2;     
      console.log("hello1");  
      addRow(colSpan);
    }
    context.lineWidth = 1;
    context.stroke();
  }

  for (let c = col_start + colSpan; c < (col_inc + col_start + rowSpan*1000); c += colSpan){
    context.moveTo(c, 0);
    context.lineTo(c, (col_inc + col_start + rowSpan*1000));  

    //Code for coloring first coloumn in old_login.js

    colh = getColHeader(colh);
    let colHeader = document.createElement('p');
    colHeader.id = 'colh' ;
    colHeader.innerHTML = colh;
    divCol.append(colHeader);
    context.lineWidth = 1;
    context.stroke();
  }
  context.save();
}

//Scroll Event listener
window.addEventListener('scroll', function(){
  const divCol = document.getElementById('div-col-header');
  const divRow = document.getElementById('div-row-header');

  //On scrolling horizontally fix position of divRow
  if(window.pageYOffset > 0){
    //divRow.classList.add('affix'); 
    divRow.style.position = 'absolute';
    divCol.style.position = 'fixed';
  }

  //On scrolling vertically fix position of divCol  
  else if(window.pageXOffset > 0){
    //divCol.classList.add('affix'); 
    divRow.style.position = 'fixed';
    divCol.style.position = 'absolute';
  }
});
  
//Functions to be handled while adding new rows
//Replace row_start with rowCnt in this function which will be the number of row from where to start
function addRow(cur_row, cur_col){
  let rowCnt = document.getElementById('row-count').value; 
  const [context, canvas]= getCanvas();
  context.font = "10px Helvetica";
  canvas.height += rowCnt*rowSpan;
  context.save();
  //Stroking the path
  let r = cur_row*rowSpan; 
  context.moveTo(0, r + rowSpan);
  context.lineTo(cur_col*colSpan, r + rowSpan);

  //Adding row header background
  context.fillStyle = "#E9E9E9";
  context.fillRect(0, r, colSpan, rowSpan);
  //Adding row header
  context.textAlign = 'center';
  context.fillStyle = "#000000";
  context.fillText(cur_row + 1, Math.ceil(0.5*colSpan), r + rowSpan - 5);
  context.stroke();

  for (let c = 0; c < cur_col*colSpan; c += colSpan){
    context.moveTo(c, cur_row*rowSpan);
    context.lineTo(c, cur_row*rowSpan + rowSpan);   
  }
  context.stroke();
  context.save();
  //updating height of canvas
  canvas.height += row_start*rowSpan;     
}

//Functions to be handled while adding new rows or cols
//Replace col_start with colCnt in this function which will be the number of row from where to start
function addCol(cur_row, cur_col){
  //Get current number of col
  const colCnt = document.getElementById('col-count').value;
  const [context, canvas] = getCanvas();
  const divCol = document.getElementById('div-col-header');
  //Update width of the canvas to incorporate new coloumns
  canvas.width += cur_col*colSpan;
  context.save();
  //Stroke several horizontal lines
  for(let r = 0; r < cur_row*rowSpan ; r+=rowSpan){
    context.moveTo(cur_col*colSpan, r);
    context.lineTo(cur_col*colSpan + colSpan, r);
    context.stroke();
  }

  //Stroke one vertical line
  const c = cur_col*colSpan + colSpan;
  context.moveTo(c, 0);
  context.lineTo(c, cur_row*rowSpan);   

  //Add col header 
  colh = getColHeader(colh);
  let colHeader = document.createElement('p');
  colHeader.id = 'colh' ;
  colHeader.innerHTML = colh;
  divCol.append(colHeader);
  context.lineWidth = 1;
  context.stroke();   
  context.save();
  //Update width of the canvas
  canvas.width += cur_col*colSpan;
}


//Generates coloumn headers
function getColHeader(header) {

  let headerLen = header.length;
  if (header == "") {
    return "A";
  } 
  else if (headerLen == 1 && header != "Z") {
    return getNextChar(header);
  } 
  else if (headerLen > 1 || header == "Z") {
    let arrHeader = header.split("").reverse();
    if (arrHeader[0] == "Z") {

    for (var i = 0; i < headerLen; i++) {
      arrHeader[i] = getNextChar(arrHeader[i]);
      if (arrHeader[i] != "Z") break;
    }

    arrHeader = arrHeader.reverse();
    arrHeader.push("A");
  } 
  else {
    arrHeader[0] = getNextChar(arrHeader[0]);
    arrHeader.reverse();
  }
  return arrHeader.join("");
  }
}

  
function getNextChar(alpha) {
  let arrColHeader = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
  if (alpha == "Z") {
    return "A";
  } 
  else {
    return arrColHeader[arrColHeader.indexOf(alpha) + 1];
  }
}



//window.addEventListener('resize', );
const canvas = getCanvas(2);

canvas.addEventListener("click", function(e){
  const divCont = document.createElement('div');
  divCont.id = 'divCont';
  body.append(divCont);
  const [x, y] = getCursorPosition(canvas, e);
  const textBox = document.createElement("input");
  textBox.setAttribute('type', 'text');
  textBox.setAttribute('id', 'input');
  textBox.style.top = y + 'px';
  textBox.style.left = x + 'px';
  textBox.style.width = colSpan - 5;
  textBox.style.height = rowSpan - 5;
  divCont.appendChild(textBox);
}); 
 
//Gets cursors current position on click
function getCursorPosition(canvas, event) {
  const rect = canvas.getBoundingClientRect();
  const x = event.clientX - rect.left ;
  const y = event.clientY - rect.top ;
  return [x, y];
}





